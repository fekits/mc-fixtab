import McFloor from 'mc-floor';
import McFixed from 'mc-fixed';
import mcScrollTo from 'mc-scrollto';

// 转换为DOM
function toDom(str) {
  let div = document.createElement('div');
  if (typeof str == 'string')
    div.innerHTML = str;
  return div.children[0];
}

// 判断入参类型
let getType = (obj) => {
  return Object.prototype.toString.call(obj).replace(/(\[object )([^\]]*)(\])/, '$2');
};

let McFixTab = function (param = {}) {
  let { navsInfo = {}, floorInfo = {}, fixedInfo = {}, scrollInfo = {}, on = {} } = param;
  let { floors = [] } = floorInfo;
  // 获取楼层
  if (getType(floors) === 'Array') {
    // 传入一个数组
    this.floors = floors;
  } else if (getType(floors) === 'NodeList') {
    // 传入多个节点
    this.floors = Array.prototype.slice.call(floors);
  } else if (getType(floors) === 'HTMLDivElement') {
    // 传入一个节点
    this.floors = [floors];
  } else if (getType(floors) === 'String') {
    // 传入一个字符
    this.floors = document.querySelectorAll(floors) || [];
  } else {
    this.floors = [];
  }

  this.scrollInfo = Object.assign({ then: () => {} }, scrollInfo);

  // 导航配置
  this.navsInfo = Object.assign({ on: { click: () => {}, expand: () => {} } }, navsInfo);

  // 楼层配置
  this.floorInfo = Object.assign({ topOffset: 50, bottomOffset: .5 }, floorInfo);

  // 固定配置
  this.fixedInfo = Object.assign({ fixedClass: 'is-fixed' }, fixedInfo);

  // 事件集合
  this.on = Object.assign({ init: () => {}, fixed: () => {}, expand: () => {}, reduced: () => {}, change: () => {}, refresh: () => {} }, on);

  // 节点集合
  this.el = {};

  // 导航容器
  this.el.area = navsInfo.area || document.body;

  // 生成导航
  this.el.root = toDom('<div class="mc-fixtab"></div>');
  this.el.wrap = toDom('<div class="mc-fixtab-wrap"></div>');
  this.el.navs = toDom('<ul class="mc-fixtab-navs"></ul>');
  this.el.wrap.appendChild(this.el.navs);
  this.el.root.appendChild(this.el.wrap);
  this.el.area.appendChild(this.el.root);

  // 为导航设置悬浮插件
  this.fixedInfo = Object.assign(this.fixedInfo, {
    el: this.el.root,
    fixedClass: this.fixedInfo.fixedClass,
    on: {
      fixed: (_this) => {
        this.on.fixed(_this);
      },
      reduced: (_this) => {
        this.on.reduced(_this);
      }
    }
  });
  this.myFixed = new McFixed(this.fixedInfo);

  // 是否生成展开按钮?
  let setExpand = (state) => {
    this.el.root.setAttribute('data-expand', state);
    this.navsInfo.expandState = state;
    this.navsInfo.on.expand(state);
  };
  if (this.navsInfo.expand) {
    this.el.expand = toDom('<div class="mc-fixtab-expand"></div>');
    this.el.root.appendChild(this.el.expand);
    let { expandState } = this.navsInfo;
    setExpand(expandState);
    this.el.expand.addEventListener('click', () => {
      expandState = parseInt(this.el.root.getAttribute('data-expand'));
      if (expandState === 1 || expandState === '1') {
        setExpand(0);
      } else {
        setExpand(1);
      }
      this.myFixed.refresh();
    });
  }
  // 刷新导航
  this.refresh(() => {
    // console.log('init');
    this.on.init();
  });
};

// 添加楼层
McFixTab.prototype.add = function (floor, cb = () => {}) {
  if (floor) {
    this.floors.push(floor);
    this.refresh(cb);
  }
};

// 删除楼层
McFixTab.prototype.del = function (floor, cb = () => {}) {
  if (floor) {
    this.floors = this.floors.filter((item) => {
      return item !== floor;
    });
    this.refresh(cb);
  }
};

McFixTab.prototype.refresh = function (cb = () => {}) {
  // console.log(this.floors);
  // 节点子集类数组转数组
  this.floors = Array.prototype.slice.call(this.floors);

  // 根据楼层位置重新排序
  this.floors.sort((a, b) => {
    return a.getBoundingClientRect().top - b.getBoundingClientRect().top;
  });

  this.el.navs.innerHTML = '';

  this.floors.map((item) => {
    let navName = item.getAttribute('data-nav');
    let navItem = toDom(`<li>${navName}</li>`);
    navItem.onclick = (ev) => {
      // 标记状态为非空闲
      // this.free = 0;
      this.myFloor.free = 0;
      this.navsInfo.on.click(item, this, ev);
      mcScrollTo({
        animationDistance: 500,
        to: {
          y: item
        },
        offset: {
          y: -this.el.root.offsetHeight - this.myFixed.top
        },
        time: 300,
        then: (pos) => {
          this.scrollInfo.then(pos);
        }
      });
      setTimeout(() => {
        this.myFloor.free = 1;
      }, 300);

    };
    this.el.navs.appendChild(navItem);
  });

  this.el.navList = this.el.navs.children;
  if (this.myFloor) {
    this.myFloor.destroy();
  }
  // 监听楼层
  this.floorInfo = Object.assign(this.floorInfo, {
    el: this.floors,
    topOffset: this.floorInfo.topOffset,
    bottomOffset: this.floorInfo.bottomOffset,
    on: {
      change: (res) => {
        this.el.activeNav = this.el.navList[res.activeIndex];

        // 清除所有导航的当前活动状态标记
        let navsLength = this.el.navList.length;
        for (let i = 0; i < navsLength; i++) {
          this.el.navList[i].className = '';
        }

        // 处理当前楼层对应导航
        if (res.activeFloor) {
          this.el.activeNav.className = 'active';
          // 滚动当前导航到中间
          mcScrollTo({
            el: this.el.wrap,
            to: {
              x: this.el.activeNav.offsetLeft - (this.el.wrap.offsetWidth / 2)
            },
            offset: {
              x: this.el.activeNav.offsetWidth / 2
            }
          });
        }
        this.on.change(this);
      }
    }
  });
  // console.log(this);
  this.myFloor = new McFloor(this.floorInfo);
  this.on.refresh();
  cb();
};

McFixTab.prototype.destroy = function () {
  // console.log('destroy');
  this.el.area.innerHTML = '';
};

export default McFixTab;
