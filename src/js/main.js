import '../css/main.scss';

// 代码着色插件
import McTinting from 'mc-tinting';

new McTinting();

let eFloors = document.querySelectorAll('.track_floor');

import McFixTab from '../../lib/mc-fixtab';

let myTab = new McFixTab({
  floorInfo: {
    floors: eFloors
  },
  scrollInfo: {
    then: () => {
      console.log('滚动了');
    }
  },
  fixedInfo: {
    keptArea: true,
    // top: 50
  },
  navsInfo: {
    area: document.getElementById('floor_nav'),
    expand: true,
    expandState: 0,
    on: {
      click(_this, aa, bb) {
        console.log(aa);
        console.log(bb);
      },
      expand(status) {
        console.log('展开状态-> ', status);
      }
    }
  },
  on: {
    fixed(_this) {
      console.log('fixed', _this);
    },
    reduced(_this) {
      console.log('reduced', _this);
    },
    change(_this) {
      console.log('change', _this, _this.myFloor);
    }
  }
});

setTimeout(() => {
  console.log(eFloors[0]);
  myTab.del(eFloors[0], () => {
    console.log('删除了F1', myTab);
  });
}, 10000);

setTimeout(() => {
  console.log(document.getElementById('f1'));
  myTab.add(document.getElementById('f1'), () => {
    console.log('添加了F1', myTab);
  });
  myTab.add(document.getElementById('f8'), () => {
    console.log('添加了F8', myTab);
  });
}, 20000);

setTimeout(() => {
  // myTab.destroy();
  myTab = null;
}, 30000);
