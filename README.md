# MC-FIXTAB
```$xslt
根据楼层自动生成楼层相关联的导航。
```

#### 索引
* [演示](#演示)
* [参数](#参数)
* [示例](#示例)
* [版本](#版本)
* [反馈](#反馈)


#### 演示
[https://junbo.name/plugins/mc-fixtab](https://junbo.name/plugins/mc-fixtab/)


#### 开始

下载项目: 

```npm
npm i mc-fixtab
```

#### 参数
```$xslt
param               {Object}    入参为一个对象

param.floors        {Array | NodeList}   DOM节点选择器，可以是id,class,name,data自定义属性等等
param.navArea       {String}             导航容器
param.floorInfo     {Object}             参考mc-floor插件的入参，详情请见https://gitlab.com/fekits/mc-floor
param.fixedInfo     {Object}             参考mc-fixed插件的入参，详情请见https://gitlab.com/fekits/mc-fixed
param.scrollToInfo  {Object}             参考mc-scrollto插件的入参，详情请见https://gitlab.com/fekits/mc-fixed
param.on            {Object}             回调事件集
```

#### 示例

```javascript
// 引入插件
import McFixtab from 'mc-fixtab';

// 示例用的跟楼层关联的导航
let eFloors = document.querySelectorAll('.track_floor');

import McFixTab from 'mc-fixtab';

let myTab = new McFixTab({
  floors: eFloors,
  navArea: document.getElementById('floor_nav'),
  fixedClass: 'is-fixed',
  on: {
    fixed(_this) {
      console.log(_this);
    }
  }
});
```

#### 版本
```$xslt
v1.1.9
1. 重构插件提高性能与可靠性
```

```$xslt
v1.1.8
1. 为展开收缩按钮添加一个回调事件on.expand
```

```$xslt
v1.1.7
1. 更新了mc-fixed插件
```

```$xslt
v1.1.5
1. 新增导航展开按钮 navsInfo: {expand: true, expandDefaultStatus: 0} expand:是否开启展开按钮？ expandDefaultStatus：默认展开或收起
```

```$xslt
v1.0.8
1. 优化可靠性。
```

```$xslt
v1.0.7
1. add,del新增回调函数。
```

```$xslt
v1.0.0
1. 核心功能完成。
```

#### 反馈
```$xslt
如果您在使用中遇到问题，请通过以下方式联系我。
QQ: 860065202
EMAIL: xiaojunbo@126.com
```
